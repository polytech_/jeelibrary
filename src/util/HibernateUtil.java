package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

    public static EntityManager emInit() {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("test");

        return entityManagerFactory.createEntityManager();

    }
}
