package dao;

import model.Book;
import util.HibernateUtil;

import javax.persistence.EntityManager;

import java.util.Collection;
import java.util.Optional;


public class BookDAO {

    private static EntityManager em  = HibernateUtil.emInit();

    public Collection<Book> getPublicBooks() {
        return em.createQuery("select b from Book b", Book.class).getResultList();
    }

    public Optional<Book> findBookById(int bookId) {
        return Optional.ofNullable(em.find(Book.class, bookId));
    }

}
