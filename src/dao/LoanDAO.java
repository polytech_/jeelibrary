package dao;

import model.Book;
import model.Loan;
import model.User;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class LoanDAO {

    private static final int MAX_LOANS = 5;
    private BookDAO bookDAO =  new BookDAO();

    private static EntityManager em  = HibernateUtil.emInit();

    public List<Loan> getLoansByUser(User user) {
        return em.createQuery("select loan from Loan loan where loan.user = :user", Loan.class)
                .setParameter("user", user)
                .getResultList();
    }

    public void loanBook(User user, Book book) {
        if (bookDAO.findBookById(book.getIdBook()).isPresent()) {
            em.getTransaction().begin();
            Loan loan = new Loan(user, book, LocalDate.now());
            em.persist(loan);
            //bookDAO.findBookById(book.getIdBook()).get().setDisponibility("Non");
            em.getTransaction().commit();
        }
    }
}
