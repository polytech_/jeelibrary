package controller;

import dao.UserDAO;
import model.User;
import org.hibernate.service.spi.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginController")
public class LoginController extends HttpServlet {

    private UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String p = request.getParameter("action");

        if(userDAO.loginIn(username, password)) {
            request.getSession().setAttribute("userConnected", new User(username, password));
            request.getRequestDispatcher("/loans").forward(request, response);
            //response.sendRedirect("/loans");
        }
        else {
            request.setAttribute("error", "Identifiants incorrects !");
            request.getRequestDispatcher("logIn.jsp").forward(request, response);
            //response.sendRedirect("logIn.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
