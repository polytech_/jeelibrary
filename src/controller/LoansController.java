package controller;

import dao.BookDAO;
import dao.LoanDAO;
import dao.UserDAO;
import model.Book;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoansController")
public class LoansController extends HttpServlet {

    private LoanDAO loanDAO;
    private UserDAO userDAO;
    private BookDAO bookDAO;

    @Override
    public void init() throws ServletException {
        loanDAO = new LoanDAO();
        userDAO = new UserDAO();
        bookDAO = new BookDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("action").equals("signIn")) {
            doGet(request, response);
        }

        else {
            HttpSession session = request.getSession(false);
            int idBook = Integer.parseInt(request.getParameter("book"));
            String user = request.getParameter("user");

            Book bookToLoan = bookDAO.findBookById(idBook).get();
            User loaner = userDAO.getUserByName(user).get();

            if(userDAO.getUserByName(user).isPresent() && bookDAO.findBookById(idBook).isPresent()) {

                loanDAO.loanBook(loaner, bookToLoan);
                User userConnected = (User)session.getAttribute("userConnected");
                request.setAttribute("myLoans", loanDAO.getLoansByUser(userConnected));
                request.getRequestDispatcher("myLoans.jsp").forward(request, response);

            }

        }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("action").equals("signIn")) {
            HttpSession session = request.getSession(false);
            if(session!=null) {
                User userConnected = (User)session.getAttribute("userConnected");
                request.setAttribute("myLoans", loanDAO.getLoansByUser(userConnected));
                request.getRequestDispatcher("myLoans.jsp").forward(request, response);
            }
            else {
                response.sendRedirect("logIn.jsp");
            }

        }
        else {
            doPost(request, response);
        }



    }
}
