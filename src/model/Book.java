package model;

import javax.persistence.*;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idBook;
    private String title;
    private String author;
    @Enumerated(EnumType.STRING)
    private Type type;
    private int copies;
    private String disponibility;


    public Book(){

    }

    public Book(String title, String author, Type type, String disponibility, int copies) {
        this.title = title;
        this.author = author;
        this.type = type;
        this.copies = copies;
        this.disponibility = disponibility;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public void setDisponibility(String disponibility) {
        this.disponibility = disponibility;
    }

    public String getDisponibility() {
        return disponibility;
    }
}
