package listener;

import model.Book;
import model.Loan;
import model.Type;
import model.User;
import util.HibernateUtil;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.time.LocalDate;

@WebListener()
public class startupListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    // Public constructor is required by servlet spec
    public startupListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {
      /* This method is called when the servlet context is
         initialized(when the Web application is deployed).
         You can initialize servlet context related data here.*/

       /* EntityManager em = HibernateUtil.emInit();

        //Utilisateurs
        User user1 = new User();
        user1.setPassword("password");
        user1.setUsername("aouname");

        User user2 = new User();
        user2.setPassword("password");
        user2.setUsername("tcuname");

        User user3 = new User();
        user3.setPassword("password");
        user3.setUsername("jluname");

        //Livres
        Book book1 = new Book();

        book1.setAuthor("Fiddlesticks");
        book1.setDisponibility("Oui");
        book1.setType(Type.THRILLER);
        book1.setTitle("Le monde de fiddle");
        book1.setCopies(120);


        Book book2 = new Book();

        book2.setAuthor("Twisted Fate");
        book2.setDisponibility("Oui");
        book2.setType(Type.AVENTURE);
        book2.setTitle("Jeu de cartes");
        book2.setCopies(200);


        Book book3 = new Book();

        book3.setAuthor("Zed");
        book3.setDisponibility("Oui");
        book3.setType(Type.AVENTURE);
        book3.setTitle("Les aventures du ninja");
        book3.setCopies(20);

        Book book4 = new Book();

        book4.setAuthor("Miss Fortune");
        book4.setDisponibility("Oui");
        book4.setType(Type.ROMANCE);
        book4.setTitle("Histoire d'amour");
        book4.setCopies(12);

        Book book5 = new Book();

        book5.setAuthor("Volibear");
        book5.setDisponibility("Non");
        book5.setType(Type.AVENTURE);
        book5.setTitle("Au fin fond du Groenland");
        book5.setCopies(40);

        Book book6 = new Book();

        book6.setAuthor("Nunu");
        book6.setDisponibility("Oui");
        book6.setType(Type.SF);
        book6.setTitle("Quand tout a commencé");
        book6.setCopies(56);


        Loan loan1 = new Loan();

        loan1.setBook(book1);
        loan1.setDate(LocalDate.of(2020, 3, 15));
        loan1.setUser(user1);


        Loan loan2 = new Loan();

        loan2.setBook(book2);
        loan2.setDate(LocalDate.of(2020, 6, 12));
        loan2.setUser(user2);


        Loan loan3 = new Loan();

        loan3.setBook(book4);
        loan3.setDate(LocalDate.of(2019, 12, 2));
        loan3.setUser(user1);


        Loan loan4 = new Loan();

        loan4.setBook(book5);
        loan4.setDate(LocalDate.of(2019, 5, 13));
        loan4.setUser(user2);


        em.getTransaction().begin();

        em.persist(user1);
        em.persist(user2);
        em.persist(user3);

        em.persist(book1);
        em.persist(book2);
        em.persist(book3);
        em.persist(book4);
        em.persist(book5);
        em.persist(book6);

        em.persist(loan1);
        em.persist(loan2);
        em.persist(loan3);
        em.persist(loan4);

        em.getTransaction().commit();*/


    }


}
