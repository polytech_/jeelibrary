<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accueil</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet"  href="./css/style.css">
</head>

<style>
    td.fit,
    th.fit {
        white-space: nowrap;
        width: 1%;
    }
</style>
<body>

    <jsp:include page="filter.jsp"/>

    <div class="container-fluid" style="width: 70%">
        <br>
        <h3>Liste  des livres dispo :</h3>
        <br>
        <table id="mainTable" class="table table-hover table-striped">
            <thead class="thead-dark">
            <tr>
                <th style="width: 25%" scope="col">Titre</th>
                <th style="width: 20%" scope="col">Auteur</th>
                <th style="width: 20%" scope="col">Type</th>
                <th style="width: 20%" scope="col">Exemplaires</th>
                <th style="width: 15%" scope="col">Disponibilité</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${publicBooks}" var="book">
                <tr>
                    <td>${book.title}</td>
                    <td>${book.author}</td>
                    <td>${book.type}</td>
                    <td>${book.copies}</td>
                    <td >${book.disponibility}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


        <div class="row justify-content-center">
            <div class="col-2">
                <a href="${pageContext.request.contextPath}/logIn.jsp" class="btn btn-primary stretched-link">Se connecter</a>
            </div>

        </div>






<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
